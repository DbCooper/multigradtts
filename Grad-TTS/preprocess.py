import argparse
import os
from glob import glob
from pathlib import Path
import tgt
import text

from utils import parse_filelist


def process_utterance(tg_path, basename, spk_id=None):
    textgrid = tgt.io.read_textgrid(tg_path)
    phone = get_alignment(
        textgrid.get_tier_by_name("phones")
    )
    t = "{" + " ".join(phone) + "}"
    if spk_id:
        return "|".join([basename, t, spk_id])
    return "|".join([basename, t])


def get_alignment(tier):
    sil_phones = ["sil", "sp", "spn"]

    phones = []
    end_idx = 0
    for t in tier._objects:
        s, e, p = t.start_time, t.end_time, t.text

        # Trim leading silences
        if phones == []:
            if p in sil_phones:
                continue
            else:
                start_time = s

        if p not in sil_phones:
            # For ordinary phones
            phones.append(p)
            end_idx = len(phones)
        else:
            # For silent phones
            phones.append(p)

    # Trim tailing silences
    phones = phones[:end_idx]

    return phones


if __name__ == '__main__':
    use_text_grid = int(os.getenv("USE_TEXT_GRID", 1))
    if use_text_grid == 1:
        parser = argparse.ArgumentParser()
        parser.add_argument("--text_grid_folder", type=str, required=False)
        parser.add_argument("--out_filelist", type=str, required=False)
        parser.add_argument("--repath", type=str, required=False)
        args = parser.parse_args()

        datasets = []
        flag = False
        for i in glob(args.text_grid_folder + "/*"):
            if os.path.isdir(i):
                flag = True
                break

        if not flag:
            for txg in glob(os.path.join(args.text_grid_folder, "*.TextGrid")):
                name = Path(txg).stem
                # name = name.split("-")[-1]
                name = name[5:].replace("-", "_").replace(" ", "_")
                path = os.path.join(args.repath, f"{name}.wav")
                p = process_utterance(txg, path)
                datasets.append(p)
        else:
            for spk_id, spk in enumerate(os.listdir(args.text_grid_folder)):
                for txg in glob(os.path.join(spk, "*.TextGrid")):
                    name = Path(txg).stem
                    # name = name.split("-")[-1]
                    path = os.path.join(args.repath, f"{spk}/{name}.wav")
                    p = process_utterance(txg, path, spk)
                    datasets.append(p)

        split_train = int(len(datasets) * 0.85)
        split_eval = int(len(datasets) * 0.95)
        with open(os.path.join(args.out_filelist, "train.txt"), "w") as fw:
            fw.writelines([f + "\n" for f in datasets[:split_train]])

        with open(os.path.join(args.out_filelist, "valid.txt"), "w") as fw:
            fw.writelines([f + "\n" for f in datasets[split_train:split_eval]])

        with open(os.path.join(args.out_filelist, "test.txt"), "w") as fw:
            fw.writelines([f + "\n" for f in datasets[split_eval:]])
    else:
        parser = argparse.ArgumentParser()
        parser.add_argument("--out_extension", default="cleaned")
        parser.add_argument("--text_index", default=1, type=int)
        parser.add_argument("--filelists", nargs="+", default=["resources/filelists/mydata/valid.txt",
                                                               "resources/filelists/mydata/train.txt"])
        parser.add_argument("--text_cleaners", nargs="+", default=["basic_cleaners"])

        args = parser.parse_args()

        for filelist in args.filelists:
            print("START:", filelist)
            filepaths_and_text = parse_filelist(filelist)
            for i in range(len(filepaths_and_text)):
                original_text = filepaths_and_text[i][args.text_index]
                cleaned_text = text._clean_text(original_text, args.text_cleaners)
                filepaths_and_text[i][args.text_index] = cleaned_text

            new_filelist = filelist + "." + args.out_extension
            with open(new_filelist, "w", encoding="utf-8") as f:
                f.writelines(["|".join(x) + "\n" for x in filepaths_and_text])
