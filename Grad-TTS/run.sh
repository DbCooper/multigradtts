#!/bin/bash

echo "Use text grid: $USE_TEXT_GRID"
echo "use MultiSpeaker: $USE_MULTI_SPEAKER"
echo "Starting Preprocess Data"

if [ "1" -eq "$USE_TEXT_GRID" ]; then
  echo "processing text grid"
  python3 preprocess.py --text_grid_folder / \
    --out_filelist / \
    --repath /
else
  echo "processing phoneminzer"
  python3 preprocess.py --filelists resources/filelists/mydata/train.txt resources/filelists/mydata/valid.txt resources/filelists/mydata/test.txt
fi

echo "Ending Preprocess Data"
echo "Starting Training"
if [ "1" -eq "$USE_MULTI_SPEAKER" ]; then
  echo "MULTI SPEAKER"
  python3 train_multi_speaker.py
else
  echo "SINGLE SPEAKER"
  python3 train.py
fi
