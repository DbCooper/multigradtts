### 1. Single Speaker instance
Format file:
> **path_to_wav_file | transcript**

### 2. Multiple Speaker instance
Format file:

> **path_to_wav_file | transcript | speaker_id**


*Note Multiple Speaker*:
- With using `phonemizer` file use for processing base format:
> **path_to_wav_file | transcript | speaker_id**

- With using `TextGrid` folder use for processing base format:
    + root/ <br>
      | speaker_1/x.TextGrid <br>
      | speaker_2/x.TextGrid <br>
      |... <br>
      | speaker_n/x.TextGrid
