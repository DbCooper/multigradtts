""" from https://github.com/keithito/tacotron """

import os
from text import cmudict

use_text_grid = int(os.getenv("USE_TEXT_GRID", 1))

if use_text_grid == 1:
    _pad = '_'
    _punctuation = ';:,.!?¡¿—…"«»“”()– '
    _special = '-'
    _letters = '0123546789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    _silences = ["@sp", "@spn", "@sil", "@spc", "@cmm"]
    # Prepend "@" to ARPAbet symbols to ensure uniqueness:
    _arpabet = ['@' + s for s in cmudict.valid_symbols]

    # Export all symbols:
    symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters) + _arpabet + list(_silences)
else:
    _pad = '_'
    _punctuation = ';:,.!?¡¿—…"«»“”()– '
    _special = '-'
    _letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    # Phonemes
    _vowels = 'iyɨʉɯuɪʏʊeøɘəɵɤoɛœɜɞʌɔæɐaɶɑɒᵻ'
    _non_pulmonic_consonants = 'ʘɓǀɗǃʄǂɠǁʛ'
    _pulmonic_consonants = 'pbtdʈɖcɟkɡqɢʔɴŋɲɳnɱmʙrʀⱱɾɽɸβfvθðszʃʒʂʐçʝxɣχʁħʕhɦɬɮʋɹɻjɰlɭʎʟ'
    _suprasegmentals = 'ˈˌːˑ'
    _other_symbols = 'ʍwɥʜʢʡɕʑɺɧ'
    _diacrilics = 'ɚ˞ɫ'
    _extra_phons = ['g', 'ɝ', '̃', '̍', '̥', '̩', '̯', '̪',
                    '͡']  # some extra symbols that I found in from wiktionary ipa annotations

    # Export all symbols:
    symbols = list(
        _pad + _letters + _punctuation + _special + _vowels + _non_pulmonic_consonants
        + _pulmonic_consonants + _suprasegmentals + _other_symbols + _diacrilics) + _extra_phons
