""" from https://github.com/keithito/tacotron """
import os
import re
from unidecode import unidecode
from phonemizer import phonemize
from .numbers import normalize_numbers

use_text_grid = int(os.getenv("USE_TEXT_GRID", 1))
_whitespace_re = re.compile(r'\s+')

_abbreviations = [(re.compile('\\b%s\\.' % x[0], re.IGNORECASE), x[1]) for x in [
    ('mrs', 'misess'),
    ('mr', 'mister'),
    ('dr', 'doctor'),
    ('st', 'saint'),
    ('co', 'company'),
    ('jr', 'junior'),
    ('maj', 'major'),
    ('gen', 'general'),
    ('drs', 'doctors'),
    ('rev', 'reverend'),
    ('lt', 'lieutenant'),
    ('hon', 'honorable'),
    ('sgt', 'sergeant'),
    ('capt', 'captain'),
    ('esq', 'esquire'),
    ('ltd', 'limited'),
    ('col', 'colonel'),
    ('ft', 'fort'),
]]


def expand_abbreviations(text):
    for regex, replacement in _abbreviations:
        text = re.sub(regex, replacement, text)
    return text


def expand_numbers(text):
    return normalize_numbers(text)


def lowercase(text):
    return text.lower()


def collapse_whitespace(text):
    return re.sub(_whitespace_re, ' ', text)


def convert_to_ascii(text):
    return unidecode(text)


def basic_cleaners(text):
    text = lowercase(text)
    if use_text_grid == 1:
        text = collapse_whitespace(text)
        return text
    else:
        phonemes = phonemize(text, language='vi', language_switch="remove-flags",
                             backend='espeak', strip=True,
                             njobs=4, punctuation_marks=';:,.!?¡¿—…"«»“”()–- ',
                             preserve_punctuation=True, with_stress=False)
        phonemes = collapse_whitespace(phonemes)
        return phonemes
